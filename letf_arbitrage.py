import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.arima.model import ARIMA
import random
import json


def main():
    etf_csv = 'parsed_data/qqq_20231026.csv'
    letf_csv = 'parsed_data/tqqq_20231026.csv'

    # Function Definitions
    def moving_average(data, window_size):
        return data['price'].rolling(window=window_size).mean()

    def exponential_smoothing(data, alpha):
        return data['price'].ewm(alpha=alpha).mean()

    # Read ETF and LETF data
    etf_df = pd.read_csv(etf_csv, header=None, names=['timestamp', 'ticker', 'order_size', 'price'])
    letf_df = pd.read_csv(letf_csv, header=None, names=['timestamp', 'ticker', 'order_size', 'price'])

    # Convert 'timestamp' to pandas.Timestamp
    etf_df['timestamp'] = pd.to_datetime(etf_df['timestamp'])
    letf_df['timestamp'] = pd.to_datetime(letf_df['timestamp'])

    # Sort data by timestamp
    etf_df.sort_values(by='timestamp', inplace=True)
    letf_df.sort_values(by='timestamp', inplace=True)

    # Initialize variables
    profit = 0
    profits = []
    letf_position = False
    letf_buy_price = 0
    i, j = 1, 1

    etf_up = False
    order_size = 1000

    ma_window = 1
    curr_window = 0

    # Parameters for statistical models
    ma_window = 5
    es_alpha = 0.3

    stop_loss = 0.0025
    stop_loss_price = 0

    ma_weight = 0.2
    es_weight = 0.2
    trend_weight = 0.6

    min_latency = 0  # 10 milliseconds
    max_latency = 0  # 50 milliseconds

    # Iterate through both datasets
    while i < len(etf_df) and j < len(letf_df):
        etf_row = etf_df.iloc[i]
        signal_strength = 0
        # Apply statistical models
        if i >= ma_window:
            ma_signal = moving_average(etf_df.iloc[i-ma_window:i], ma_window).iloc[-1]
            es_signal = exponential_smoothing(etf_df.iloc[i-ma_window:i], es_alpha).iloc[-1]

            # Calculate MA and ES signals
            signal_strength += ma_weight if etf_row['price'] > ma_signal else 0
            signal_strength += es_weight if etf_row['price'] > es_signal else 0

        # Calculate the trend signal
        if i > 0 and etf_row['price'] > etf_df.iloc[i-1]['price']:
            signal_strength += trend_weight

        # Determine the overall trend based on signal strength
        etf_up = signal_strength >= 0.5
            
        if etf_up:
            if not letf_position:
                #open a position set to the next letf fill price
                while letf_df.iloc[j]['timestamp'] < etf_row['timestamp']: j += 1 #increment until we can react to the trend reversal
                letf_buy_price = letf_df.iloc[j]['price']
                letf_position = True
                
                #calculate stop loss
                stop_loss_price = letf_buy_price*(1-stop_loss)
            else:
                pass #hold current pos
        else:
            # Get the current time where we observe trend reversal
            reversal_time = pd.to_datetime(etf_row['timestamp'])

            # Generate a random latency
            latency = random.randint(min_latency, max_latency)  # in milliseconds

            # Add latency to the reversal time
            reversal_time_with_latency = reversal_time + pd.Timedelta(milliseconds=latency)
            
            if letf_position:
                #close position
                while letf_df.iloc[j]['timestamp'] < reversal_time_with_latency: j += 1 #increment until we can react to the trend reversal
                reversal_letf_price = letf_df.iloc[j]['price']
                fill_price = max(reversal_letf_price, stop_loss_price)
                profit += (fill_price - letf_buy_price) * order_size
                letf_position = False
                profits.append(profit)
                print(f"profit: {profit}")
            else:
                pass #ignore, not doing shorts
            
        if letf_position: j += 1
        i += 1

    filename = "backtesting_results/weighted_trend"

    with open(f"{filename}.txt", "w") as f:
        f.write(f"profit: {profit}\n")
        f.write(f"stop_loss: {stop_loss}\n")
        f.write(f"ma_window: {ma_window}\n")
        f.write(f"es_alpha: {es_alpha}\n")
        f.write(f"min_latency: {min_latency}\n")
        f.write(f"max_latency: {max_latency}\n")
        f.write(f"ma_weight: {ma_weight}\n")
        f.write(f"es_weight: {es_weight}\n")
        f.write(f"trend_weight: {trend_weight}\n")
        
    # Save profits to a JSON file
    profits_data = {
        "profits": profits,
        "parameters": {
            "stop_loss": stop_loss,
            "ma_window": ma_window,
            "es_alpha": es_alpha,
            "min_latency": min_latency,
            "max_latency": max_latency,
            "ma_weight": ma_weight,
            "es_weight": es_weight,
            "trend_weight": trend_weight
        }
    }
    
    with open(f"{filename}.json", "w") as json_file:
        json.dump(profits_data, json_file)

    print("plotting profits...")
    # Plotting the profit
    plt.plot(profits)
    plt.xlabel('Time (Data Points)')
    plt.ylabel('Cumulative Profit')
    plt.title('LETF Arb Profit Over Time')
    plt.savefig(f'{filename}.png')
    # plt.show()
    
if __name__ == "__main__":
    main()