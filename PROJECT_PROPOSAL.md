**<span style="text-decoration:underline;">Group 6 Project Proposal: LETF Lag-Lead</span>**



* **Technology**
    * Our core strategy will be written in C++ in combination with Strategy Studio to easily backtest. Any prototyping and basic visualizations will be done using Python and Pandas/Matplotlib. 
* **Assets & Symbols**
    * We will focus on the QQQ & TQQQ for strategy because TQQQ is the most liquid LETF. If we have more time, we will test different leveraged and unleveraged ETFs such as SPY & UPRO to try and take advantage of more inefficiencies. Given more time, we will also test 
* **Data Source**
    * We will parse data from IEX through the professor’s scripts.
* **Date Ranges:**
    * We will run and backtest our strategy using data from the past three months as we found that to be a good middle ground between too much data size and not enough backtesting. 
* **Frameworks for CI/CD**
    * GitHub:
        * Usage:  Assign tasks and discuss issues. Each commit will be discussed during the meeting. Building and deployment processes.
        * Rationale: GitHub Actions provide a good integration with our repository, enabling better management of CI/CD pipelines directly within the GitHub ecosystem.
    * Python Testing:
        * Usage: For writing and testing our basic prototype on Python code (e.g., data analysis, visualization scripts).
        * Rationale: Pytest offers a straightforward and flexible framework for testing trading strategies and ML tools, crucial for ensuring the reliability of our prototyping and ensuring our logic works before coding in C++.	
* **Timeline**
    * Week 1:
        * Core Strategy Development in C++.
        * Python scripts for prototyping and visualization.
    * Week 2:
        * Integration of Strategy Studio.
        * Initial backtesting with the QQQ & TQQQ datasets.
    * Week 3:
        * Enhancing Strategy Logic: Refining trading algorithms based on backtest results.
        * Testing: Continuous unit testing, integration testing, and performance testing.
    * Week 4:
        * Optimization: Optimizing code for performance and accuracy.
        * Final Backtesting: Extensive backtesting, including different market conditions and leveraging machine learning for predictions.
        * Finalize code and project
        * Time to fix any bugs or errors
    * Week 5:
        * Documentation and Reporting: Preparing final reports and documentation.
* **Goals**
    * Bare Minimum Target:
        * Extremely basic lag-lead implementation running in C++ on Strategy Studio with at least a month of QQQ and TQQQ data. 
    * Expected Completion Goals:
        * Well-tested lag-lead algorithm tested on both QQQ/TQQQ and SPY/UPRO. 
        * Tinkering parameters to create a more sophisticated algorithm while ensuring minimal latency hit.
        * Explanations for the results of the backtest as well as analyze on how latency and technology influences our strategy.
    * Reach Goals
        * Train a neural network to find out the best intervals to take advantage of lagging in leveraged ETF tracking and finetune parameters as a result.
        * Analyze 2x leverage as well as apply the strategy to futures and other highly leveraged instruments.
* **Individual Contribution**
    * **Rohit** (Strategy, Finetuning):
        * Core Strategy Development in C++
            * Develop the core trading strategy logic in C++ 
            * Coordinate with Adarsh to ensure that the Python prototypes align with the core strategy
        * Finding opportunities
            * Analyze new LETF pairs and test out different leverage levels to see correlations with inefficient tracking.
    * **Fang Yi** (Strategy, basic ML, Optimizations):
        * Data Parsing and Management
            * Set up IEX parsing code from professor
        * Testing and Optimization
            * Lead the continuous unit testing, integration testing, and performance testing
            * Work closely with Rohit and Adarsh to ensure that code logic is sound and bug-free
            * Build testing framework and code for testing in a HFT environment with varying arbitrary random latencies
            * Continue to improve and develop strategy to be more resilient to latencies and different arbitrary environmental hyperparams.
        * Finding Opportunities
            * Find out new interesting strategies and iterative improvements that we can introduce onto our basic lead-lag strategy and LETFs and figure out how to implement them with the rest of the team
            * Identifying and implementing complementary strategies that we can use to pick LETF/ETF pairs
    * **Adarsh** - (Development, basic ML)
        * Visualization
            * Python code and simple visualization plotting for trading strategy performance
        * Aid in Development of Core Strategy in C++
            * Work closely with Rohit and Sam for seamless development in strategy and backtesting of our core strategy in C++
        * Enhancing Strategy Logic
            * Prototype ML and other strategy enhancements
            * Exploring new avenues in algorithmic trading and implementation
    * **Sam** (Integration, Backtesting)
        * Lead the integration of Rohit’s C++ prototypes in Week 2 with Strategy Studio with backtesting
        * Manage Backtesting pipeline and ensure that backtesting happens consistently and well.

		



* **External Resources**
    * Financial Data Sources
        * we will need to take a look at getting data from other exchanges where the pairs that we are looking at trade with greater volume.
    * Machine Learning for Financial Markets
        * we will need this to build an effective RNN/LSTM for predicting the best intervals for lead-lag trading.
* **Final Deliverable**
    * Our final deliverable will include a data-backed analysis on the potential of deploying a lag-lead strategy taking advantage of inefficiencies with leverage tracking on a low-latency scale. 
    * This will include:
        * A well organized, well documented repository and README.md explaining the strategy and how to use all the code.
        * Documentation on Strategy Overview
        * Visualizations of potential P&L after trading a LETF/ETF pair on at least 3 months worth of data.
        * Detailed analysis of the microstructure that enables this as well as the foundations of a leveraged ETF
        * Tabulated overview on which pairs were the most profitable with our final strategy
        * Analysis on how latency affected the profitability of our back tested strategies.
        * Future Improvements and Recommendations that we think we might implement given a longer time frame.
* **Team Member Future Plans**
    * Fang Yi Lin - Graduating Spring 2025.
    * Rohit Lakshman - Graduating Spring 2025.
    * Adarsh Dayalan - Graduating Spring 2024. Planning on pursuing Masters at UIUC
    * Sam Sun -  Graduating Fall 2024.
* **Finite State Machine**

![Trading Logic FSM](https://i.ibb.co/qsWQP9R/90ae8d33-ee65-4e36-8d7c-af26d1b513c0.png)