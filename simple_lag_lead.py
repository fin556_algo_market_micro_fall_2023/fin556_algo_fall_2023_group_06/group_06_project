import pandas as pd
import matplotlib.pyplot as plt

# Initialize variables
cash = 100000  # Starting cash
position = 0   # No initial position
portfolio_value = cash  # Initial portfolio value is all in cash

portfolio_values = []  # Store portfolio value over time
trade_points = []  # Store trade points for visualization

# Define a simple moving average function
def SMA(data, window):
    return data.rolling(window=window).mean()

def buy(price, quantity):
    global cash, position, portfolio_value
    cost = price * quantity
    if cash >= cost:
        cash -= cost
        position += quantity
        portfolio_value = cash + (position * price)
        trade_points.append(('Buy', price))
    else:
        print("Not enough cash to buy")

def sell(price, quantity):
    global cash, position, portfolio_value
    if position >= quantity:
        cash += price * quantity
        position -= quantity
        portfolio_value = cash + (position * price)
        trade_points.append(('Sell', price))
    else:
        print("Not enough shares to sell")

# Read the CSV file
file_path = "iex_parsed/tqqq_20231026.csv"

try:
    df = pd.read_csv(file_path, header=None, names=['timestamp', 'symbol', 'quantity', 'price'])
except FileNotFoundError:
    print("File not found")
    exit()

# Sorting the dataframe by timestamp
df = df.sort_values(by='timestamp')

# Calculate the simple moving average
window_size = 5  # Define the window size for SMA
df['SMA'] = SMA(df['price'], window_size)

for i in range(window_size, len(df)):
    prev_price = df.iloc[i - 1]['price']
    curr_price = df.iloc[i]['price']
    curr_sma = df.iloc[i]['SMA']
    
    trade_size = int(df.iloc[i]['quantity'])

    if curr_price > curr_sma:
        buy(curr_price, trade_size)
    elif curr_price < curr_sma:
        sell(curr_price, trade_size)

    portfolio_values.append(portfolio_value)  # Track portfolio value over time

# Close any open positions at the last known price
if position > 0:
    sell(df.iloc[-1]['price'], position)

# Plotting
plt.figure(figsize=(12, 6))
plt.plot(portfolio_values, label='Portfolio Value')
plt.title('Portfolio Value Over Time')
plt.xlabel('Number of Trades')
plt.ylabel('Portfolio Value ($)')

# Marking buy/sell points
for point in trade_points:
    trade_type, price = point
    color = 'g' if trade_type == 'Buy' else 'r'
    plt.scatter(trade_points.index(point), price, color=color, label=trade_type if trade_type not in plt.gca().get_legend_handles_labels()[1] else "")

plt.legend()
plt.grid(True)
plt.show()
