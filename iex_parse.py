import sys
from iex_parser import Parser, TOPS_1_6
import time

def main(tickers):
    sample_file = "iex_source/data_feeds_20231205_20231205_IEXTP1_TOPS1.6.pcap.gz"
    files = {ticker: open(f"parsed_data/{ticker}_20231205.csv", "w") for ticker in tickers}

    start_time = time.time()
    
    total_count = 0
    ticker_counts = {ticker: 0 for ticker in tickers}

    with Parser(sample_file, TOPS_1_6) as reader:
        for message in reader:
            total_count += 1
            if total_count % 50000 == 0:
                for ticker_count in ticker_counts.items():
                    print(f"{ticker_count[0]}: {ticker_count[1]}")
            if message['type'] == "trade_report":
                ticker = message['symbol'].decode('utf-8')
                if ticker in tickers:
                    ticker_counts[ticker] += 1
                    f = files[ticker]
                    timestamp = int(message['timestamp'].timestamp() * 1e9)
                    f.write(f"{timestamp},{message['symbol']},{message['size']},{message['price']}\n")

    # Close all files
    for f in files.values():
        f.close()

    print(f"Time taken: {time.time() - start_time}")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python script_name.py TICKER1,TICKER2,...")
    else:
        tickers = set(sys.argv[1].split(','))
        main(tickers)
