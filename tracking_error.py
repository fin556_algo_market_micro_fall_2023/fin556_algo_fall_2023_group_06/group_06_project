import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import pandas as pd
import numpy as np

# Reload the data
file_path_qqq = 'iex_parsed/qqq_20231026.csv'
file_path_tqqq = 'iex_parsed/tqqq_20231026.csv'
qqq_data = pd.read_csv(file_path_qqq, names=['timestamp', 'symbol', 'volume', 'price'])
tqqq_data = pd.read_csv(file_path_tqqq, names=['timestamp', 'symbol', 'volume', 'price'])

# Convert timestamp to datetime and set as index
qqq_data['timestamp'] = pd.to_datetime(qqq_data['timestamp'])
qqq_data.set_index('timestamp', inplace=True)
tqqq_data['timestamp'] = pd.to_datetime(tqqq_data['timestamp'])
tqqq_data.set_index('timestamp', inplace=True)

# Calculate percentage price changes
qqq_data['qqq_pct_change'] = qqq_data['price'].pct_change() * 100
tqqq_data['tqqq_pct_change'] = tqqq_data['price'].pct_change() * 100

# Convert timestamp to float for interpolation
qqq_timestamps_as_float = (qqq_data.index - pd.Timestamp("1970-01-01")) // pd.Timedelta('1s')
tqqq_timestamps_as_float = (tqqq_data.index - pd.Timestamp("1970-01-01")) // pd.Timedelta('1s')

# Interpolate TQQQ data to match QQQ timestamps
interpolator = interp1d(tqqq_timestamps_as_float, tqqq_data['tqqq_pct_change'], fill_value='extrapolate', bounds_error=False)
qqq_data['actual_tqqq_pct_change'] = interpolator(qqq_timestamps_as_float)

# Calculate expected TQQQ change (3x QQQ)
qqq_data['expected_tqqq_pct_change'] = 3 * qqq_data['qqq_pct_change']

# Calculate percentage tracking error
qqq_data['tracking_error_pct'] = qqq_data['actual_tqqq_pct_change'] - qqq_data['expected_tqqq_pct_change']

# Plotting the tracking error
import matplotlib.dates as mdates

# Plotting the tracking error with adjusted x-axis for a single day
plt.figure(figsize=(12, 6))
qqq_data['tracking_error_pct'].plot()

# Format the x-axis to show hours and minutes
plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval=1))  # Interval can be adjusted as needed
plt.gcf().autofmt_xdate()  # Auto-format the x-axis labels for readability

plt.title('Second-by-Second Tracking Error Percentage (TQQQ vs. 3x QQQ) on October 26, 2023')
plt.xlabel('Time (Hours:Minutes)')
plt.ylabel('Tracking Error (%)')
plt.axhline(y=0, color='r', linestyle='--')
plt.grid(True)
plt.show()
