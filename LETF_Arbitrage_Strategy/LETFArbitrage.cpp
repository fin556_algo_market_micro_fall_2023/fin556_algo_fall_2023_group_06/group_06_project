// ExchangeArbitrageStrategy.cpp

#include "ExchangeArbitrageStrategy.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <chrono>

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;

using namespace std;

// Constructor
LETFArbitrage::LETFArbitrage(StrategyID strategyID, 
                                                     const string& strategyName, 
                                                     const string& groupName) : 
                                                     Strategy(strategyID, strategyName, groupName),
                                                     profit(0),
                                                     letf_position(false),
                                                     letf_buy_price(0),
                                                     ma_window(5),
                                                     es_alpha(0.3),
                                                     stop_loss(0.0025),
                                                     stop_loss_price(0),
                                                     order_size(1000),
                                                     min_latency(10),
                                                     max_latency(50) {
    // Additional initialization if necessary
}

// Destructor
LETFArbitrage::~ExchangeArbitrageStrategy() {
}

void LETFArbitrage::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate) {    
    for (SymbolSetConstIter it = symbols_begin(); it != symbols_end(); ++it) {
        eventRegister->RegisterForBars(*it, BAR_TYPE_TIME, 1);
    }
}

void LETFArbitrage::DefineStrategyParams() {
    // Define strategy parameters
}

void LETFArbitrage::OnResetStrategyState() {
    // Reset strategy state
}

void LETFArbitrage::OnTrade(const TradeDataEventMsg& msg) {
    const Instrument* instrument = &msg.instrument();
    double bid_price = msg.instrument().top_quote().bid();

    //calculate the signals
    auto ma_signal = calculateMovingAverage(etf_prices, ma_window);
    auto es_signal = calculateExponentialSmoothing(etf_prices, es_alpha);

    //calculate the signal strength
    double signal_strength = 0;
    signal_strength += (bid_price > ma_signal) ? ma_weight : 0;
    signal_strength += (bid_price > es_signal) ? es_weight : 0;

    //check if we have a position in the LETF
    if (letf_position) {

        //check if we need to sell the LETF
        if (bid_price > letf_buy_price * (1 + stop_loss)) {

            //sell the LETF
            SendOrder(instrument, -order_size);

            letf_position = false;

            profit += (bid_price - letf_buy_price) * order_size;
        }
    } else {

        //check if we need to buy the LETF
        if (bid_price < letf_buy_price * (1 - stop_loss)) {

            //buy the LETF
            SendOrder(instrument, order_size);

            letf_position = true;

            letf_buy_price = bid_price;
        }
    }
}

void LETFArbitrage::OnBar(const BarEventMsg& msg) {
    // Implement bar event handling
}

double LETFArbitrage::calculateMovingAverage(const vector<double>& prices, int windowSize) {
    if (prices.size() < windowSize) return 0;

    double sum = accumulate(prices.end() - windowSize, prices.end(), 0.0);
    
    return sum / windowSize;
}

double LETFArbitrage::calculateExponentialSmoothing(const vector<double>& prices, double alpha) {
    double result = prices.front();

    for (auto price = prices.begin() + 1; price != prices.end(); ++price) {

        result = alpha * (*price) + (1 - alpha) * result;
    }
    return result;
}

void LETFArbitrage::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
    // Implement order update event handling
}

void LETFArbitrage::OnMarketState(const MarketStateEventMsg& msg) {
    // Implement market state event handling
}

void LETFArbitrage::OnParamChanged(StrategyParam& param) {
    // Implement parameter change handling
}

void LETFArbitrage::SendOrder(const Instrument* instrument, int trade_size)
{
    double price;

    curr_price = instrument->top_quote().bid();

    // Create an order object with the specified parameters
    OrderParams params(
                    *instrument,     // Instrument to trade
                    abs(trade_size_each_time), // Absolute value of trade size
                    curr_price,           // Price at which to trade
                    MARKET_CENTER_ID_IEX, // Market center ID
                    (trade_size_each_time > 0) ? ORDER_SIDE_BUY : ORDER_SIDE_SELL, // Order side (buy or sell)
                    ORDER_TIF_DAY,   // Time in force (how long the order is valid for)
                    ORDER_TYPE_LIMIT // Order type (limit or market)
                    );
    
    TradeActionResult tra = trade_actions()->SendNewOrder(params);
    // Check if the order was sent successfully and print a message indicating the result
}


void LETFArbitrage::OnScheduledEvent(const ScheduledEventMsg& msg) {
    
}