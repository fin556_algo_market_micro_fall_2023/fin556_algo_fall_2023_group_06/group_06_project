/*================================================================================
*     Source: ../RCM/StrategyStudio/examples/strategies/SimplePairsStrategy/SimplePairsStrategy.h
*     Last Update: 2021/04/15 13:55:14
*     Contents:
*     Distribution:
*
*
*     Copyright (c) RCM-X, 2011 - 2021.
*     All rights reserved.
*
*     This software is part of Licensed material, which is the property of RCM-X ("Company"),
*     and constitutes Confidential Information of the Company.
*     Unauthorized use, modification, duplication or distribution is strictly prohibited by Federal law.
*     No title to or ownership of this software is hereby transferred.
*
*     The software is provided "as is", and in no event shall the Company or any of its affiliates or successors be liable for any
*     damages, including any lost profits or other incidental or consequential damages relating to the use of this software.
*     The Company makes no representations or warranties, express or implied, with regards to this software.
/*================================================================================*/

#pragma once

#ifndef _STRATEGY_STUDIO_LIB_EXAMPLES_SIMPLE_PAIRS_STRATEGY_H_
#define _STRATEGY_STUDIO_LIB_EXAMPLES_SIMPLE_PAIRS_STRATEGY_H_

#ifdef _WIN32
    #define _STRATEGY_EXPORTS __declspec(dllexport)
#else
    #ifndef _STRATEGY_EXPORTS
    #define _STRATEGY_EXPORTS
    #endif
#endif

#include <Strategy.h>
#include <Analytics/ScalarRollingWindow.h>
#include <MarketModels/Instrument.h>
#include <Utilities/ParseConfig.h>
#include <boost/filesystem.hpp>
#include <vector>
#include <map>
#include <iostream>
#include <string>

using namespace RCM::StrategyStudio;
typedef Utilities::PriceToIntKeyFunc<8> DoubleHasher;

struct StrategyConfigRow {
    double ticks;
    int lots;
    double stop;
    double target;
};

class StrategyConfigGrid {
public:
    StrategyConfigGrid();
    void set_logger(StrategyLogger* logger) { logger_ = logger; }
    bool read_config(const boost::filesystem::path& filename);
    bool logger_set() const { return logger_ != nullptr; }
    StrategyConfigRow at(double change) const;
    size_t size() const { return rows_.size(); }
    bool valid() const { return valid_; }
private:    
    std::map<StrategyStudioInt64, StrategyConfigRow> rows_;
    StrategyLogger* logger_;
    bool valid_;
};

class QuoteTracker {
public:
    QuoteTracker(size_t window_size) 
        : aapl_nasdaq_bid_window_(window_size), aapl_nasdaq_ask_window_(window_size),
          aapl_iex_bid_window_(window_size), aapl_iex_ask_window_(window_size),
          amzn_nasdaq_bid_window_(window_size), amzn_nasdaq_ask_window_(window_size),
          amzn_iex_bid_window_(window_size), amzn_iex_ask_window_(window_size) {}

    void UpdateQuote(const std::string& symbol, const std::string& market_center, const std::string& side, double price) {
        if (symbol == "AAPL") {
            if (market_center == "NASDAQ") {
                UpdateWindow(side, price, aapl_nasdaq_bid_window_, aapl_nasdaq_ask_window_);
            } else if (market_center == "IEX") {
                UpdateWindow(side, price, aapl_iex_bid_window_, aapl_iex_ask_window_);
            }
        } else if (symbol == "AMZN") {
            if (market_center == "NASDAQ") {
                UpdateWindow(side, price, amzn_nasdaq_bid_window_, amzn_nasdaq_ask_window_);
            } else if (market_center == "IEX") {
                UpdateWindow(side, price, amzn_iex_bid_window_, amzn_iex_ask_window_);
            }
        }
    }

    const double GetRollingWindowElement(const std::string& symbol, const std::string& market_center, const std::string& side, size_t index) const {
        if (symbol == "AAPL") {
            if (market_center == "NASDAQ") {
                return side == "bid" ? aapl_nasdaq_bid_window_[index] : aapl_nasdaq_ask_window_[index];
            } else if (market_center == "IEX") {
                return side == "bid" ? aapl_iex_bid_window_[index] : aapl_iex_ask_window_[index];
            }
        } else if (symbol == "AMZN") {
            if (market_center == "NASDAQ") {
                return side == "bid" ? amzn_nasdaq_bid_window_[index] : amzn_nasdaq_ask_window_[index];
            } else if (market_center == "IEX") {
                return side == "bid" ? amzn_iex_bid_window_[index] : amzn_iex_ask_window_[index];
            }
        }
        return 0.0; // Default return if symbol, market center, or side doesn't match
    }


    bool IsWindowFull(const std::string& symbol, const std::string& market_center, const std::string& side) {
        if (symbol == "AAPL") {
            if (market_center == "NASDAQ") {
                return side == "bid" ? aapl_nasdaq_bid_window_.full() : aapl_nasdaq_ask_window_.full();
            } else if (market_center == "IEX") {
                return side == "bid" ? aapl_iex_bid_window_.full() : aapl_iex_ask_window_.full();
            }
        } else if (symbol == "AMZN") {
            if (market_center == "NASDAQ") {
                return side == "bid" ? amzn_nasdaq_bid_window_.full() : amzn_nasdaq_ask_window_.full();
            } else if (market_center == "IEX") {
                return side == "bid" ? amzn_iex_bid_window_.full() : amzn_iex_ask_window_.full();
            }
        }
        return false; // Default return if symbol, market center, or side doesn't match
    }


private:
    void UpdateWindow(const std::string& side, double price, 
                      Analytics::ScalarRollingWindow<double>& bid_window, 
                      Analytics::ScalarRollingWindow<double>& ask_window) {
        if (side == "bid") {
            bid_window.push_back(price);
        } else if (side == "ask") {
            ask_window.push_back(price);
        }
    }

    Analytics::ScalarRollingWindow<double> aapl_nasdaq_bid_window_;
    Analytics::ScalarRollingWindow<double> aapl_nasdaq_ask_window_;
    Analytics::ScalarRollingWindow<double> aapl_iex_bid_window_;
    Analytics::ScalarRollingWindow<double> aapl_iex_ask_window_;
    Analytics::ScalarRollingWindow<double> amzn_nasdaq_bid_window_;
    Analytics::ScalarRollingWindow<double> amzn_nasdaq_ask_window_;
    Analytics::ScalarRollingWindow<double> amzn_iex_bid_window_;
    Analytics::ScalarRollingWindow<double> amzn_iex_ask_window_;

   };

class ExchangeArbitrage : public Strategy {
public:
    typedef std::map<const Instrument*, Bar> Bars; 
    typedef Bars::iterator BarsIter;
    typedef Bars::const_iterator BarsConstIter;

public:
    ExchangeArbitrage(StrategyID strategyID, const std::string& strategyName, const std::string& groupName);
    ~ExchangeArbitrage();

public: /* from IEventCallback */
    /**
     * This event triggers whenever trade message arrives from a market data source.
     */ 
    virtual void OnTrade(const TradeDataEventMsg& msg);

    /**
     * This event triggers whenever aggregate volume at best price changes, based 
     * on the best available source of liquidity information for the instrument.
     *
     * If the quote datasource only provides ticks that change the NBBO, top quote will be set to NBBO
     */ 
    virtual void OnTopQuote(const QuoteEventMsg& msg) {}

    /**
     * This event triggers whenever a new quote for a market center arrives from a consolidate or direct quote feed,
     * or when the market center's best price from a depth of book feed changes.
     *
     * User can check if quote is from consolidated or direct, or derived from a depth feed. This will not fire if
     * the data source only provides quotes that affect the official NBBO, as this is not enough information to accurately
     * mantain the state of each market center's quote.
     */ 
    virtual void OnQuote(const QuoteEventMsg& msg) {}
    
    /**
     * This event triggers whenever a order book message arrives. This will be the first thing that
     * triggers if an order book entry impacts the exchange's DirectQuote or Strategy Studio's TopQuote calculation.
     */ 
    virtual void OnDepth(const MarketDepthEventMsg& msg);

    /**
     * This event triggers whenever a Bar interval completes for an instrument
     */ 
    virtual void OnBar(const BarEventMsg& msg);

    /**
     * This event contains alerts about the state of the market
     */
    virtual void OnMarketState(const MarketStateEventMsg& msg);

    /**
     * This event triggers whenever new information arrives about a strategy's orders
     */ 
    virtual void OnOrderUpdate(const OrderUpdateEventMsg& msg);

    /**
     * This event contains strategy control commands arriving from the Strategy Studio client application (eg Strategy Manager)
     */ 
    virtual void OnStrategyControl(const StrategyStateControlEventMsg& msg) {}

    /**
     * This event contains alerts about the status of a market data source
     */ 
    virtual void OnDataSubscription(const DataSubscriptionEventMsg& msg) {}

    /**
     * This event contains alerts about the status of the Strategy Server process
     */ 
    virtual void OnAppStateChange(const AppStateEventMsg& msg);

    /**
     *  Perform additional reset for strategy state 
     */
    void OnResetStrategyState();

    /**
     * Notifies strategy for every succesfull change in the value of a strategy parameter.
     *
     * Will be called any time a new parameter value passes validation, including during strategy initialization when default parameter values
     * are set in the call to CreateParam and when any persisted values are loaded. Will also trigger after OnResetStrategyState
     * to remind the strategy of the current parameter values.
     */ 
    void OnParamChanged(StrategyParam& param);

private: // Helper functions specific to this strategy
    void AdjustPortfolio();
    OrderID SendOrder(const Instrument* instrument, int trade_size, double price, const std::string& market_center_str);
    OrderID SendLimitOrder(const Instrument* instrument, int trade_size, double price, MarketCenterID market_center_id);


private: /* from Strategy */
    virtual void RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate); 
    
    /**
     * Define any params for use by the strategy 
     */     
    virtual void DefineStrategyParams();

    /**
     * Provides an ideal place during strategy initialization to define custom strategy graphs using graphs().series().add(...) 
     */ 
    virtual void DefineStrategyGraphs();

private:
    std::string config_path_;
    StrategyConfigGrid config_;
    QuoteTracker quote_tracker_;
    Bars bars_;
    const MarketModels::Instrument* instrument_x_;
    const MarketModels::Instrument* instrument_y_;
    double z_score_threshold_;
    int position_size_;
    int target_position_;
    bool debug_;
};

extern "C" {
    _STRATEGY_EXPORTS const char* GetType()
    {
        return "ExchangeArbitrageStrategy";
    }

    _STRATEGY_EXPORTS IStrategy* CreateStrategy(const char* strategyType,
                                   unsigned strategyID,
                                   const char* strategyName,
                                   const char* groupName) {
        if (strcmp(strategyType, GetType()) == 0) {
            return *(new ExchangeArbitrage(strategyID, strategyName, groupName));
        } else {
            return NULL;
        }
    }

    _STRATEGY_EXPORTS const char* GetAuthor() {
        return "dlariviere";
    }

    _STRATEGY_EXPORTS const char* GetAuthorGroup() {
        return "UIUC";
    }

    _STRATEGY_EXPORTS const char* GetReleaseVersion() {
        return Strategy::release_version();
    }
}

#endif
