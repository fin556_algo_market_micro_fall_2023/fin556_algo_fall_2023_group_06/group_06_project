# Strategies in Tracking Error Arbitrage: Leveraged ETF Analysis

- [Strategies in Tracking Error Arbitrage: Leveraged ETF Analysis](#strategies-in-tracking-error-arbitrage-leveraged-etf-analysis)
   * [People](#people)
   * [Project Description](#project-description)
   * [Background ](#background)
      + [Tracking Error Arbitrage and Leveraged ETFs](#tracking-error-arbitrage-and-leveraged-etfs)
      + [Application to Tracking Error Arbitrage in Leveraged ETFs](#application-to-tracking-error-arbitrage-in-leveraged-etfs)
         - [Strategic Trading](#strategic-trading)
         - [Execution of Trades](#execution-of-trades)
   * [Technologies](#technologies)
      + [Programming Languages](#programming-languages)
      + [Software](#software)
      + [Data Source](#data-source)
      + [Pipeline frameworks](#pipeline-frameworks)
      + [Packages](#packages)
   * [Strategy Development](#strategy-development)
      + [Identifying the Lead-Lag Relationship](#identifying-the-lead-lag-relationship)
      + [Modeling Price Movements](#modeling-price-movements)
      + [Strategy Formulation: Tracking Error Arbitrage in Leveraged ETFs](#strategy-formulation-tracking-error-arbitrage-in-leveraged-etfs)
      + [Risk Management in High-Frequency Trading](#risk-management-in-high-frequency-trading)
   * [Backtesting with QQQ and TQQQ](#backtesting-with-qqq-and-tqqq)
      + [Setting Up the Backtest Environment](#setting-up-the-backtest-environment)
      + [Implementing the Strategy](#implementing-the-strategy)
   * [Analysis](#analysis)
      + [Tracking Error Percentage](#tracking-error-percentage)
      + [Trade Frequency Analysis](#trade-frequency-analysis)
      + [Initial Backtest](#initial-backtest)
      + [Real Market Conditions and Adjusted Backtest](#real-market-conditions-and-adjusted-backtest)
      + [Refining Strategy](#refining-strategy)
         - [Impact of Latency on P/L](#impact-of-latency-on-pl)
         - [Improved Risk Management with Stop Loss Implementation](#improved-risk-management-with-stop-loss-implementation)
      + [Experimentation with Weighting Schemes](#experimentation-with-weighting-schemes)
         - [Observations from the Backtest](#observations-from-the-backtest)
   * [Conclusion](#conclusion)


## People

**Fang Yi, Lin**

- I am FY Lin, a Computer Science undergraduate at the Univerisity of Illinois at Urbana-Champaign. I have taken courses related to ML and am interesting in ML/deep learning, specifically NLP. My skills include C++, Python, Database Systems.
- Linkedin: https://www.linkedin.com/in/fang-yi-lin-762b31197/
- Email: fyl2@illinois.edu 

**Adarsh Dayalan**

- I am Adarsh Dayalan, a Computer Science undergraduate at the Univerisity of Illinois at Urbana-Champaign. I have taken courses related to AI, ML, and algorithms design. My skills include languages such as Python, C++, Java. I also am experienced with full stack dev, and ML research.
- Linkedin: https://www.linkedin.com/in/adarsh-dayalan/
- Email: dayalan2@illinois.edu

<img src="./team_photos/sam_photo.jpg" alt="" width="400"/>

**Tian Sun**

- I am Tian Sun, a Master of Industrial Engineering student at the University of Illinois Urbana-Champaign, set to graduate in December 2024. I have experience in data analysis for 5 years, and I’m passionate about applying analysis techniques to real-world challenges. With my studying in courses about machine learning and algorithms design, I am confident to make improvements for models with market data. My programming languages are Python, R, C++, and shell scripting, and I am willing to learn new languages.
- Linkedin: [Tian Sun](https://www.linkedin.com/in/tian-sun-426a64253/)
- Email: [tiansun2@illinois.edu](mailto:tiansun2@illinois.edu) 

**Rohit Lakshman**

- I am Rohit Lakshman, a Computer Science and Economics undergraduate at UIUC.  I was a Software Engineer Intern at Geneva Trading last summer, and will be at IMC Trading in 2024. I have taken high level economics and computer science coursework and enjoy low-level systems and markets. My skills include C++, Python, and strategy design. 
- Linkedin: https://www.linkedin.com/in/rohit-lakshman/
- Email: rohitl2@illinois.edu

## Project Description

The project takes a look at leveraged ETFs which are ripe with inefficiency due to being composed primarily of complicated swaps as well as suffering from high fees and leverage decay. As a result, the products never fully track the underlying they are intended and only promise an attempt to do so. As a result, we believe there is opportunity to profit from tracking error by deploying a high frequency strategy looking at the tick-level movements of a leveraged ETF and what it is intended to track. With enough speed and counterparty liquidity, the strategy showed consistent profitability with `little variation` and a `high Sharpe Ratio`.

## Background 

### Tracking Error Arbitrage and Leveraged ETFs

Tracking error arbitrage is particularly relevant in the context of leveraged ETFs, which are designed to provide amplified returns based on the performance of an underlying index or benchmark. Due to their leveraged nature, these ETFs are subject to unique market microstructure challenges that can result in significant tracking errors. This approach contrasts with pairs trading by emphasizing the exploitation of lag responses rather than seeking correlated pairs. The models used to identify and capitalize on tracking error arbitrage include:

1. **Moving Average (MA)**: This model can predict the expected path of a leveraged ETF by smoothing out short-term volatility and identifying the longer-term trend. For an asset $Y_t$, the moving average is computed as:

$$
MA_t = \frac{1}{n} \sum_{i=1}^{n} Y_{t-i}
$$

where $MA_t$ represents the moving average, $n$ is the number of periods, and $Y_{t-i}$ are the past observed values of the asset.

2. **Exponential Smoothing (ES)**: This approach places more weight on recent price changes, which can be crucial for leveraged ETFs that often react more dramatically to market moves: The exponential smoothing value is calculated as:

$$
ES_t = \alpha Y_t + (1 - \alpha) ES_{t-1}
$$

where $ES_t$ is the current smoothed value, &alpha; is the smoothing constant that balances recent and past data, and $Y_t$ is the current observed value.

3. **Autoregressive Integrated Moving Average (ARIMA)**: For leveraged ETFs, ARIMA models can help forecast future prices by accounting for complex price dynamics due to leverage. With the lagging asset $Y_t$, the ARIMA model is specified as:

$$
ARIMA(p, d, q): \quad (1 - \sum_{i=1}^{p} \phi_i L^i) (1 - L)^d Y_t = (1 + \sum_{i=1}^{q} \theta_i L^i) \varepsilon_t
$$

where $p$ represents the autoregressive terms, $d$ the degree of differencing, $q$ the moving average terms, $L$ the lag operator, &phi; and &theta; the parameters, and $\varepsilon_t$ the error term.

### Application to Tracking Error Arbitrage in Leveraged ETFs

Leveraged ETFs are particularly susceptible to tracking errors due to their inherent 'leveraged' design and objectives, which can lead to significant deviations from the performance of their underlying index, especially over short intervals. These tracking errors become pronounced in high-frequency trading environments where the tick-by-tick microstructure plays a crucial role. This measurement is done on a tick-by-tick basis, capturing the minutiae of price changes and allowing for a nuanced understanding of the ETF's behavior.

#### Strategic Trading

With a focus on short-term market movements, the strategy exploits these tracking errors by taking positions that are anticipated to profit from the ETF's realignment with the index. This approach is predicated on a rapid response to the tracking errors as they materialize, rather than waiting for long-term corrections.

- **Long Position**: If the leveraged ETF's price momentarily dips below the model's predicted path based on the index's latest tick, a long position is initiated in anticipation of a swift correction.
- **Short Position**: Conversely, if the ETF's price spikes above the expected trajectory, a short position is taken to capitalize on the expected downward adjustment.

#### Execution of Trades

The execution of trades is designed to be swift and responsive, aligning with the high-frequency nature of the strategy. This is achieved through:

- **Automated Trading Algorithms**: These algorithms are calibrated to detect and act upon tracking errors in real-time, ensuring that trades are executed within the narrow window of opportunity presented by the ETF's tick-by-tick price movements.
- **Advanced Statistical Models**: These models are employed to dynamically establish the expected performance of `Y_t`, accounting for the latest market data and trends.

In essence, this strategy is not simply about identifying a pair of correlated assets, as in pairs trading. Instead, it is about recognizing and acting upon the immediate discrepancies that arise from the lead-lag dynamics between a leveraged ETF and its underlying index. By leveraging time-series analysis and market microstructure knowledge, the strategy aims to extract value from the nanosecond movements of the market, aiming to provide a competitive edge in high-frequency trading scenarios.

## Technologies

### Programming Languages

- Python: 

    Python was used for strategy research before implementation in C++. It's comfortable to deal with market dataset and genarate visualizations to show the backtest profit obviously.

- C++:

    C++ was used in Strategy Studio for data parser and backtesting. It's a high performance language and is ideal for strategy implementation.

### Software

- Strategy Studio

    Strategy Studio is a software for strategy development based on C++. We used it to implement the strategy and obtained the backtest results.

### Data Source

- IEX Exchange:

    We used daily data from IEX Exchange as the main data input to backtest our strategy. Since it's intraday data, we could try to detect more trading signals. We used [iexdownloaderparser](https://gitlab.engr.illinois.edu/shared_code/iexdownloaderparser/-/tree/main?ref_type=heads) from the professor to capture intraday trading signals and implemented the strategy.
    

### Pipeline frameworks

- Gitlab:

    Gitlab was used for version control so that we can track changes on the project.

- Virtual Box/Vagrant:

    we used vagrant to set up a virtual machine connect to the professor's lab to run back test with Strategy Studio.


### Packages

Below is a list of packages that we used in Python.

- numpy
- pandas
- matplotlib
- plotly


Numpy and Pandas were used for matrix calculation and data manipulation because of their high efficiency. Matplotlib and Plotly were used to generate trading signal plots and backtest result plots. 

## Strategy Development

### Identifying the Lead-Lag Relationship

The lead-lag relationship is critical in the context of leveraged ETFs, where the tracking error can often be attributed to the structural mechanisms of these funds. The cross-correlation function (CCF) is instrumental in detecting such relationships between the ETF price series $X_t$ and the underlying index or benchmark $Y_t$. The CCF at lag &tau; is calculated as:

$$
CCF(\tau) = \frac{\sum_{t=1}^{N-\tau} (X_{t+\tau} - \bar{X})(Y_t - \bar{Y})}{\sqrt{\sum_{t=1}^N (X_t - \bar{X})^2 \sum_{t=1}^N (Y_t - \bar{Y})^2}}
$$

Where $\bar{X}$ and $\bar{Y}$ are the means of $X_t$ and $Y_t$, respectively. A pronounced peak in CCF(&tau;) at a particular &tau; suggests the existence of a lead-lag relationship pertinent to tracking error arbitrage.

### Modeling Price Movements

Leveraged ETFs are modeled to account for the compounding effects and daily rebalancing, which contribute to the tracking error. An ARIMA model is adept at capturing these dynamics:

$$
(1 - \sum_{i=1}^{p} \phi_i L^i) (1 - L)^d X_t = (1 + \sum_{i=1}^{q} \theta_i L^i) \varepsilon_t
$$

Where:

- $p$ is the autoregressive order.
- $d$ is the differencing order to achieve stationarity.
- $q$ is the moving average order.
- $L$ denotes the lag operator.
- $\varepsilon_t$ is the error term, representing the tracking error in the ETF's price movements.

### Strategy Formulation: Tracking Error Arbitrage in Leveraged ETFs

Short-term tracking error arbitrage strategies are specifically designed for leveraged ETFs to exploit the discrepancies that arise due to the unique market microstructure and the ETFs' rebalancing acts. The strategy involves:

- **Rapid Monitoring**: Leveraging high-frequency data to monitor the tracking error between the leveraged ETF $X_t$ and the underlying index $Y_t$.

- **Dynamic Positioning**: Quickly initiating long or short positions in the ETF based on its deviation from the predicted performance, derived from the ARIMA model.

The positions are determined by the magnitude and direction of the tracking error:

- **Long Position**: Initiated when the ETF's price is lower than the model's forecast, anticipating a price increase as the ETF realigns with the index.

- **Short Position**: Executed when the ETF's price exceeds the forecast, anticipating a price decline to correct the overshoot.

The trading rule, tailored for high-frequency trading, is:

$$
\Delta Y_t = \beta (\Delta X_t) - \epsilon_t
$$

where $\Delta Y_t$ represents the price change of the ETF, $\beta$ is the response coefficient, $\Delta X_t$ is the change in the index, and $\epsilon_t$ signifies the tracking error.

Trades are triggered when $|\epsilon_t|$ exceeds a certain threshold, signaling a significant tracking error that is likely to be corrected imminently.

### Risk Management in High-Frequency Trading

In high-frequency trading of leveraged ETFs, risk management is refined to address the specific risks associated with tracking error arbitrage:

- **Stop-Loss Orders**: These are set at tight thresholds to mitigate the risks of rapid price movements in volatile markets.
  
$$
\text{Stop-loss price} = \text{Entry price} \times (1 - \text{Stop-loss percentage})
$$

- **Real-Time Risk Metrics**: Measures such as VaR are calculated on a rolling basis to continuously assess exposure and adjust strategies accordingly.

## Backtesting with QQQ and TQQQ

We started simulating the strategy using historical data to evaluate its potential effectiveness. For the tracking error arbitrage strategy focused on leveraged ETFs, we included the following steps by analyzing the behavior of QQQ (an ETF that tracks the NASDAQ-100 Index) and TQQQ (a leveraged ETF aiming to return three times the daily performance of the QQQ).

### Setting Up the Backtest Environment

- **Data Acquisition**: Secured access to historical tick-by-tick data for QQQ and TQQQ. Given that we are testing a high-frequency trading strategy, it's crucial to use granular data, down to nanoseconds. This data must be comprehensive, including all trades, quotes, and order book changes.

- **Strategy Studio Setup**: Utilized a powerful backtesting platform called Strategy Studio, which can handle large datasets (terabytes of data) and simulate trading with high precision. Ensure that the platform can process nanosecond timestamps and accurately reflect the market microstructure at that resolution.

### Implementing the Strategy

- **Data Processing**: Input the high-frequency data into Strategy Studio, normalizing timestamps, and ensuring synchronization between QQQ and TQQQ data streams.

- **Model Calibration**: Integrate the ARIMA models and any other statistical tools used to identify the lead-lag relationship and the tracking error into the backtesting environment. Calibrate these models using a portion of the data (in-sample) to optimize the strategy's parameters.

- **Execution Simulation**: Simulate the strategy's execution logic, which involves initiating trades based on the predicted tracking error and executing them within the constraints of the backtest environment, considering factors like position sizing and liquidity.

- **Performance Metrics**: Evaluate the strategy's performance using metrics such as net profit, Sharpe ratio, maximum drawdown, and win/loss ratio. Assess the strategy's ability to capitalize on the inefficiencies of TQQQ in relation to QQQ.

- **Experimental Modifications**: Iterate on results and try experiments where key parameters of the ARIMA models and other statistical tools are systematically varied. This includes experimenting with different lag times, moving average windows, stop loss, and thresholds for initiating trades.

## Analysis

### Tracking Error Percentage
Our investigation into the feasibility of a tracking error arbitrage strategy commenced with an analysis of the tracking error percentage between leveraged ETFs, specifically TQQQ and its benchmark QQQ. The aim was to uncover any micro inefficiencies present in the market that could be exploited for trading opportunities.

The initial phase of our analysis involved plotting the tracking error percentage over time. The results, as depicted in the provided graph, indicate that the tracking error fluctuates within a band of $0.2\%$, occasionally spiking to as high as $1\%$. This variance is indicative of tracking inefficiencies between TQQQ and QQQ and underscores the potential for an arbitrage strategy to capitalize on these discrepancies.

<p align="center">
    <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_06/group_06_project/-/raw/main/backtesting_results/Error_tracking.png?ref_type=heads"> 
</p>

### Trade Frequency Analysis
Upon implementing our strategy, we closely monitored the number of trades executed per second. This measure is crucial as it provides insight into the operational tempo of our strategy and its alignment with high-frequency trading principles, which is essential when attempting to exploit tracking errors. Our findings show a consistent execution of trades, aligning with the occurrence of tracking errors, thereby validating our approach to leveraging these brief windows of inefficiency.

<p align="center">
    <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_06/group_06_project/-/raw/main/backtesting_results/tick_results.png?ref_type=heads"> 
</p>

### Initial Backtest
Our examination of the tracking error arbitrage strategy began with a straightforward backtest focusing on leveraged ETFs. The backtest was predicated on a simple yet pivotal rule: execute a trade whenever the tracking error discrepancy exceeded a threshold of $0.1\%$. The profit and loss (P/L) from this initial test were charted over a one-week period to assess the strategy's efficacy.

The initial backtest yielded amazing results, with the strategy delivering $100\%$ returns under perfect market conditions. This graph represents the ideal scenario where trades are executed instantly at the observed price.

<p align="center">
    <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_06/group_06_project/-/raw/main/backtesting_results/initial_test.png?ref_type=heads"> 
</p>

While this test served as an excellent proof of concept, showcasing the potential of the strategy under optimal conditions, it did not account for the realities of the market, such as liquidity constraints and order execution latency.

### Real Market Conditions and Adjusted Backtest
To create a more accurate representation of live trading, we introduced variables that simulate real market conditions, including liquidity depth and latency from order dispatch to fulfillment. The adjusted backtest with a starting capital of $\$10,000$ offers a stark contrast to our initial results.

<p align="center">
    <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_06/group_06_project/-/raw/main/backtesting_results/with_stoploss.png?ref_type=heads"> 
</p>

Under these conditions, the strategy closed with a P/L of $-\$146$. This outcome highlights the critical impact that real-world trading frictions can have on a high-frequency strategy. The discrepancy between the idealized and realistic backtests underscores the necessity of accounting for the nuances of market microstructure when developing trading strategies.

### Refining Strategy
Our backtesting process for the tracking error arbitrage strategy underwent meticulous refinement, adjusting for factors such as latency and incorporating risk management techniques to enhance performance.

#### Impact of Latency on P/L
One of the critical refinements was to simulate the effect of latency on the strategy's profitability. By introducing a latency of $100$ milliseconds, we observed a consequential impact on the P/L. This adjustment led to a performance dip ranging from $2\% \to 7\%$, which translates to an absolute underperformance of $\$100\to\$500$ on an initial investment of $\$10,000$ over the course of a week.

<p align="center">
    <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_06/group_06_project/-/raw/main/backtesting_results/with_and_no_latency.png?ref_type=heads"> 
</p>

This finding is critical as it demonstrates the sensitivity of high-frequency strategies to execution delays. A latency of merely one-tenth of a second can significantly diminish the returns, highlighting the need for ultra-low latency trading systems when employing such strategies.

#### Improved Risk Management with Stop Loss Implementation
Building on the insights gained from the latency adjustments, we turned our attention to risk management to bolster the strategy's robustness. The introduction of a stop-loss threshold set at $0.25\%$ was aimed at curbing potential losses during adverse market movements.

The implementation of this refined risk management approach proved to be beneficial. The revised backtest showed a marked improvement in the strategy's performance, with the P/L divergence over time becoming more favorable. Specifically, the strategy that previously recorded a P/L of $-\$1200$ improved to $-\$250$, representing a significant reduction in losses and a relative P/L improvement of $10\%$.

<p align="center">
    <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_06/group_06_project/-/raw/main/backtesting_results/with_and_no_stoploss.png?ref_type=heads"> 
</p>

### Experimentation with Weighting Schemes

In our continuous effort to refine the tracking error arbitrage strategy for leveraged ETFs, we explored the impact of varying the weighting schemes within our moving average and exponential smoothing models. Adjusting these weights is crucial as it affects how much precedence is given to recent versus older price data, thereby influencing the sensitivity of the strategy to recent market movements.

Our experimentation involved two distinct weighting configurations: one that favored a longer-term trend (weighted_trend) and another that placed more emphasis on recent signals (weighted_signals). The graph below illustrates the cumulative profit over time for both runs.

<p align="center">
    <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_06/group_06_project/-/raw/main/backtesting_results/different_weights.png?ref_type=heads"> 
</p>

#### Observations from the Backtest
The backtest results revealed the following:

- The strategy employing a longer-term weighted average (blue line) displayed a pattern of reaching higher peaks, suggesting a better capture of prolonged market trends.

- Conversely, the strategy that focused on recent signals (orange line) reacted more quickly to market changes, which resulted in a more volatile performance with frequent but smaller peaks and troughs.

- Neither strategy consistently outperformed the other, indicating that the optimal weighting scheme may depend on market conditions and the specific objectives of the trading strategy.

## Conclusion

This study has systematically explored the viability of a tracking error arbitrage strategy within the domain of leveraged ETFs. The quantitative analysis was underpinned by a suite of time-series models, rigorous backtesting procedures, and risk management techniques, all applied with a focus on high-frequency trading mechanisms.

Initial backtests, conducted under idealized conditions, suggested substantial profitability for the arbitrage strategy, with returns reaching 100% in 1 week. However, subsequent tests incorporating real-world factors such as latency and liquidity constraints yielded more conservative results, thereby emphasizing the importance of market microstructure in strategy efficacy.

The latency simulation indicated a proportional negative impact on profitability, with an increase of $100$ milliseconds resulting in a $2\%\to7\%$ reduction in P/L. This highlighted the criticality of execution speed within the high-frequency trading framework and substantiated the investment in state-of-the-art trading infrastructure.

Further refinement of the strategy through the implementation of a stop-loss threshold at $0.25\%$ markedly improved the risk-return profile. This adjustment mitigated the magnitude of potential losses, enhancing the strategy's resilience to market volatility and reducing the drawdown by $10\%$.

Experiments adjusting the weighting parameters in the moving average and exponential smoothing models showcased the sensitivity of the strategy's performance to the selected time horizons. Neither the longer-term trend focus nor the emphasis on recent signals consistently outperformed the other, indicating a dependency on prevailing market conditions and the need for dynamic model calibration.

In conclusion, the findings affirm that while tracking error arbitrage presents a compelling opportunity for profit in leveraged ETF markets, its success is contingent upon a nuanced understanding of market microstructure, the agility of execution, and robust risk mitigation practices. These conclusions will serve as guidelines for the deployment of the strategy in live-market conditions, where continuous monitoring and adaptive strategy tuning are imperative for sustained performance.

This report encapsulates the collaborative efforts and analytical rigor of Fang Yi, Lin, Adarsh Dayalan, Tian Sun, and Rohit Lakshman, presenting a comprehensive account of the tracking error arbitrage strategy's development and validation for academic and practical application in financial markets.
